//
//  HomeCoordinator.swift
//  News
//
//  Created by Rosy on 12/9/20.
//

import Foundation
import Combine

final class HomeCoordinator: BaseCoordinator {
    
    /// The main App Route
    private let route: Route

    /// Initializer
    init(route: Route) {
        self.route = route
        super.init()
        
    }
    
    /// Method that will start the initial app flow
    /// - Parameter deepLink: the deeplink if needed
    override func start(with deepLink: DeepLink?) {
        if deepLink == nil {
            showHome()
        } else {
            switch deepLink! {
            default: break
            }
        }
    }
    
    /// show the home screen
    private func showHome() {
        /// instantiate
        let view = HomeView()
        let viewModel = HomeViewModel()
        let controller = HomeController(baseView: view, baseViewModel: viewModel)

        viewModel.trigger.receive(on: RunLoop.main).sink { [weak self] (route) in
            guard let self = self else { return }
           self.handleRouteTrigger(route)
        }.store(in: &viewModel.bag)
        
        route.setRoot(controller)
    }
    
    private func showArticle(newsArticle: NewsArticle) {
        /// instantiate
        let view = ArticleView()
        let viewModel = ArticleViewModel(newsArticle: newsArticle)
        let controller = ArticleController(baseView: view, baseViewModel: viewModel)
        
        viewModel.trigger.receive(on: RunLoop.main).sink { [weak self] (route) in
            guard let self = self else { return }
           self.handleRouteTrigger(route)
        }.store(in: &viewModel.bag)
        
        route.push(controller, animated: true)
    }
    
    
    /// Handles the trigger for app routing
    /// - Parameter trigger: the routable trigger
    private func handleRouteTrigger(_ trigger: AppRoutable) {
        switch trigger {
        case AppRoute.home:
            showHome()
        case AppRoute.article(let newsArticle):
            showArticle(newsArticle: newsArticle)
        default: break
        }
    }
}
