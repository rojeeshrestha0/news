//
//  HomeView.swift
//  News
//
//  Created by Rosy on 12/10/20.
//

import Foundation
import UIKit

class HomeView: BaseView {
    /// collection view
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        // layout.itemSize.height = 10
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: NewsCell.identifier)
        collectionView.backgroundColor = .white
//        collectionView.layer.borderWidth = 1
//        collectionView.layer.borderColor = UIColor.systemGray.cgColor
        collectionView.accessibilityIdentifier = "NewsCollection"
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    override func create() {
        super.create()
        generateDesign()
    }
}
extension HomeView {
    func generateDesign() {
        addSubview(collectionView)
        
        ///Constraint activation for the collectionView
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20.0),
            collectionView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}
