//
//  NewsCell.swift
//  News
//
//  Created by Rosy on 12/10/20.
//

import Foundation
import UIKit
import Kingfisher

protocol Selection: class {
    func selectedArticle(article: NewsArticle)
}

class NewsCell: UICollectionViewCell {
    ///T/he imag eview of the article
    lazy var newsImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        return imageView
    }()
    
    ///Continue reading button option
    lazy var readMoreButton: UIButton = {
        let button = UIButton()
        button.setTitle("Read more ...", for: .normal)
        //        button.layer.cornerRadius = 20
        button.contentHorizontalAlignment = .right
        //button.semanticContentAttribute = .forceRightToLeft
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
//        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        button.addTarget(self, action: #selector(actionButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .red
        return button
    }()
    
    ///The heading label o fthe news
    lazy var headingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = 0
        return label
    }()
    
    ///The description o rthe content for the news
    lazy var descrptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        return label
    }()
    
    ///Delegate for the article selectiom
    weak var delegate: Selection?
    
    var articleModel: NewsArticle! {
        didSet {
            configureCell()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        create()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func  create()  {
        contentView.backgroundColor = .clear
        contentView.addSubview(newsImage)
        contentView.addSubview(headingLabel)
       // contentView.addSubview(descrptionLabel)
        contentView.addSubview(readMoreButton)
        
        
        NSLayoutConstraint.activate([
            ///constraint for Image
            newsImage.topAnchor.constraint(equalTo: contentView.topAnchor),
            newsImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            newsImage.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            newsImage.heightAnchor.constraint(equalTo: newsImage.widthAnchor, multiplier: 9 / 16),
            
            /// tile and article label constraint
            headingLabel.topAnchor.constraint(equalTo: newsImage.bottomAnchor, constant: 2),
            headingLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 14),
            headingLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
//            /// description label constraint
//            descrptionLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 2),
//            descrptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 14),
//            descrptionLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
//
            ///  read more button Constraint
            readMoreButton.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 2),
            readMoreButton.heightAnchor.constraint(equalToConstant: 20),
            readMoreButton.widthAnchor.constraint(equalToConstant: 130),
            readMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 18),
            readMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        ])
        
        readMoreButton.addTarget(self, action: #selector(actionButton), for: .touchUpInside)
    }
    
    private func configureCell() {
                let text = "\(articleModel.title)" + "\n\n" + "\(articleModel.content)"
                let firstRange = (text as NSString).range(of: articleModel.title)
                let secondRange = (text as NSString).range(of: articleModel.content)
        
                let attributeString = NSMutableAttributedString(string: text)
                attributeString.addAttributes([.foregroundColor: UIColor.black, .font: UIFont.boldSystemFont(ofSize: 16)], range: firstRange)
                attributeString.addAttributes([.foregroundColor: UIColor.black, .font: UIFont.systemFont(ofSize: 14) ], range: secondRange)
                headingLabel.attributedText = attributeString

        newsImage.setImage(string: articleModel.urlToImage)//"https://example.com/image.png")
    }
    
    @objc private func actionButton() {
        delegate?.selectedArticle(article: articleModel)
    }
}


