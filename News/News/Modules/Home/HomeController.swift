//
//  HomeController.swift
//  News
//
//  Created by Rosy on 12/9/20.
//

import Foundation
import UIKit

class HomeController: BaseController {
    
    /// The view
    private lazy var screenView: HomeView = {
        return self.baseView as! HomeView //swiftlint:disable:this force_cast
    }()
    
    /// The viewModel
    private lazy var viewModel: HomeViewModel = {
        return self.baseViewModel as! HomeViewModel //swiftlint:disable:this force_cast
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        screenView.collectionView.delegate = self
        screenView.collectionView.dataSource = self
        viewModel.callApi()
        screenView.indicate = true
    }
    
    override func setupNavigationBar() {
        navigationItem.title = LocalizedKey.newsTitle.value
    }
    
    //Shows navigationbar only on this controller
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func observeEvents() {
        viewModel.manager.$articles.receive(on: RunLoop.main).sink{ [weak self] _ in
            self?.screenView.collectionView.reloadData()
            self?.screenView.indicate = false
        }.store(in: &viewModel.bag)
        
    }
}
extension HomeController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.manager.articles.count
 }
 
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NewsCell.identifier, for: indexPath) as! NewsCell
        cell.articleModel = viewModel.manager.articles[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height - collectionView.bounds.width*0.5)
        }
}

extension HomeController: Selection {
    func selectedArticle(article: NewsArticle) {
        viewModel.trigger.send(AppRoute.article(article))
    }
    
}
