//
//  HomeViewModel.swift
//  News
//
//  Created by Rosy on 12/9/20.
//

import Foundation
import Combine

class HomeViewModel: BaseViewModel {
    let manager: ArticleManager
    
    override init() {
       manager = ArticleManager()
    }
    
    func callApi() {
        let parameter = ["country": "us"]
        manager.request(router: NewsRouter.headline(parameter))
    }
}

