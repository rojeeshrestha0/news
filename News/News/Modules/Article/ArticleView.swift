//
//  ArticleView.swift
//  News
//
//  Created by Rosy on 1/12/21.
//

import Foundation
import UIKit
import WebKit

class ArticleView: BaseView {
    lazy var webView: WKWebView = {
       let view = WKWebView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func create() {
        super.create()
        generateDesign()
    }
}
extension ArticleView {
    func generateDesign() {
        addSubview(webView)
        
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            webView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            webView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            webView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -20)
        ])
    }
}
