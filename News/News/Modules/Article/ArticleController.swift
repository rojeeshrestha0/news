//
//  ArticleController.swift
//  News
//
//  Created by Rosy on 1/12/21.
//

import Foundation
import UIKit
import Combine

class ArticleController: BaseController {
    /// The view
    private lazy var screenView: ArticleView = {
        return self.baseView as! ArticleView //swiftlint:disable:this force_cast
    }()
    
    /// The viewModel
    private lazy var viewModel: ArticleViewModel = {
        return self.baseViewModel as! ArticleViewModel //swiftlint:disable:this force_cast
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupNavigationBar() {
        navigationItem.title = LocalizedKey.article.value
    }
}
