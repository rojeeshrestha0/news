//
//  ArticleViewModel.swift
//  News
//
//  Created by Rosy on 1/12/21.
//

import Foundation

class ArticleViewModel: BaseViewModel {
    let newsArticle : NewsArticle
    
    init(newsArticle: NewsArticle) {
        self.newsArticle = newsArticle
    }
}
