//
//  AppRoute.swift
//  News
//
//  Created by Rosy on 12/9/20.
//

import Foundation

enum AppRoute: AppRoutable {
    case home
    case article(NewsArticle)
}

