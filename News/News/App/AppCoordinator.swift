//
//  AppCoordinator.swift
//  News
//
//  Created by Rosy on 12/8/20.
//

import Foundation
import Combine
import UIKit

final class AppCoordinator: BaseCoordinator {
    
    /// The main route for the app
    private let route: Route

    
    /// the dispose bag
    var bag = Set<AnyCancellable>()
    
    /// Initializer
    init(route: Route) {
        self.route = route
   
        super.init()
    }
    
    /// Start the coordinator process
    override func start(with deepLink: DeepLink?) {
        runHomeCoordinator()
    
    }
    
    /// Run the home coordinator
    private func runHomeCoordinator() {
        let homeCoordinator = HomeCoordinator(route: route)
            homeCoordinator.onFinish = {[weak self] in
                guard let self = self else { return }
                self.start(with: nil)
            }
            coordinate(to: homeCoordinator)
    }
    
    
  
       
}
