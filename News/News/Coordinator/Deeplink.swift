//
//  Deeplink.swift
//  News
//
//  Created by Rosy on 12/8/20.
//

import Foundation

/// The deeplink we will use to navigate to various potion of app flow
/// This protocol will be able to identify the deeplonk from within app
public protocol DeepLink { }
