//
//  UIImageView.swift
//  News
//
//  Created by Rosy on 12/10/20.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func setImage(string: String) {
        if let url = URL(string: string) {
            self.kf.indicatorType = .activity
            self.kf.setImage(with: url)
            self.kf.setImage(with: url)

        }
    }
    
}
