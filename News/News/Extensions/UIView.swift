//
//  UIView.swift
//  News
//
//  Created by Rosy on 12/10/20.
//

import Foundation
import UIKit


extension UIView {
    
    /// The name of the view
    static var identifier: String {
        return String(describing: self)
    }
}
