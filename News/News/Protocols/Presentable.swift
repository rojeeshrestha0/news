//
//  Presentable.swift
//  News
//
//  Created by Rosy on 12/8/20.
//

import Foundation
import UIKit

/// The presentable protocol for coordinators
public protocol Presentable {
    var presenting: UIViewController? { get }
}
