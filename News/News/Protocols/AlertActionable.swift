//
//  AlertActionable.swift
//  News
//
//  Created by Rosy
//

import Foundation
import UIKit

public protocol AlertActionable {
    var title: String { get }
    var style: UIAlertAction.Style { get }
}


