//
//  NewsArticle.swift
//  News
//
//  Created by Rosy on 12/10/20.
//

import Foundation

struct NewsArticle: Codable {
    var source: Source
    var author: String
    var title: String
    var description: String
    var url: String
    var urlToImage: String
    var publishedAt: String
    var content: String
    
    
    init(from decoder: Decoder) throws {
        let  container = try decoder.container(keyedBy: CodingKeys.self)
        source =  try container.decodeIfPresent(Source.self, forKey: .source) ?? Source()
        author =  try container.decodeIfPresent(String.self, forKey: .author) ?? ""
        title =  try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        description =  try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        url =  try container.decodeIfPresent(String.self, forKey: .url) ?? ""
        urlToImage =  try container.decodeIfPresent(String.self, forKey: .urlToImage) ?? ""
        publishedAt =  try container.decodeIfPresent(String.self, forKey: .publishedAt) ?? ""
        content =  try container.decodeIfPresent(String.self, forKey: .content) ?? ""
    }
}

///The souce object that has id and name

struct Source: Codable {
    var objectId: String
    var name: String
    
    enum CodeKeys: String, CodingKey {
        case objectId = "id"
        case name
    }
    
    init() {
        objectId = ""
        name = ""
    }
    
    init(from decoder: Decoder) throws {
        let  container = try decoder.container(keyedBy: CodeKeys.self)
        objectId =  try container.decodeIfPresent(String.self, forKey: .objectId) ?? ""
        name =  try container.decodeIfPresent(String.self, forKey: .name) ?? ""
    }
}
