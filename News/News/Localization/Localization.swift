//
//  Localization.swift
//  News
//
//  Created by Rosy on 1/12/21.
//

import Foundation

private protocol Localizable {
    var key: String { get }
    var value: String { get }
}

private struct Localizer {
    static func localized(key: Localizable, bundle: Bundle = .main, tableName: String = "English", value: String = "", comment: String = "", param: String = "") -> String {
        
        let value = String(format: NSLocalizedString(key.key,tableName: tableName, bundle: bundle, value: value, comment: comment), param)
        
        return value
 }
    
}

enum LocalizedKey: Localizable {
    case newsTitle
    case article
    
    var key: String {
        switch self {
        case .newsTitle: return "NEWS_TITLE"
        case .article: return "ARTICLE"
        }
    }
    
    var value: String {
        switch self {
        default:
            return Localizer.localized(key: self)
        }
    }
    
    
}
